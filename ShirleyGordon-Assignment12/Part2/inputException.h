#pragma once
#include <exception>
#include <iostream>
#include <vector>
#include <string>

using std::vector;
using std::string;

class inputException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "Invalid input! Please enter again:";
	}

	static void checkFailState();
	static void clearIstream();
	static void getNumericInput(double& input);
	static void getNumericInput(int& input);
};

class nameInputException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "Name exception! Please don't enter the same name twice.";
	}

	static void searchName(const vector<string>& names, const string& newName);
	static void getName(vector<string>& namesVector, string& name);
};