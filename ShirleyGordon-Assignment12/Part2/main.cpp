#include "MessagesSender.h"
#include <thread>

int main(void)
{
	std::vector<string> users;
	std::queue<string> messages;

	// Start threads for queueing new messages and sending them out to the connected users.
	std::thread queueMessagesT(MessagesSender::queueMessages, std::ref(messages));
	std::thread sendMessagesT(MessagesSender::sendMessages, std::ref(messages), std::ref(users));
	
	// Display the menu and do what the user asks.
	MessagesSender::menu(users);

	// Join the two threads once user asked to exit.
	queueMessagesT.join();
	sendMessagesT.join();

	return 0;
}