#include "inputException.h"

/*
Function checks if the input stream is in fail state and throws an exception if it is.
Input: none.
Output: none.
*/
void inputException::checkFailState()
{
	if (std::cin.fail())
	{
		throw inputException();
	}
}

/*
Function gets rid of failure state of input stream.
Input: none.
Output: none.
*/
void inputException::clearIstream()
{
	std::cin.clear(); // Get rid of failure state
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Discard 'bad' character(s) 
}

/*
Function gets double input.
Input: reference to double variable.
Output: none.
*/
void inputException::getNumericInput(double& input)
{
	bool getInputAgain = false;

	do // Get input
	{
		getInputAgain = false;
		try
		{
			std::cin >> input;
			checkFailState(); // Check if the input is valid. If not, throw exception,.
		}
		catch (std::exception& e)
		{
			getInputAgain = true;
			std::cout << e.what() << std::endl; // Print exception
			clearIstream(); // Clear input stream and try again
		}
	} while (getInputAgain);
}

/*
Function gets integer input.
Input: reference to integer variable.
Output: none.
*/
void inputException::getNumericInput(int& input)
{
	double doubleInput = 0;

	getNumericInput(doubleInput); // Put input into a double variable.
	input = (int)doubleInput; // Cast to int.
}

/*
Function checks if the name entered has already been entered before,
and throws an exception if it has been.
Input: names vector reference and new name reference.
Output: none.
*/
void nameInputException::searchName(const vector<string>& names, const string& newName)
{
	unsigned int i = 0;

	// Compare new name to every past name
	for (i = 0; i < names.size(); i++)
	{
		if (names[i] == newName) // If a match is found, throw exception
		{
			throw nameInputException();
		}
	}
}

/*
Function gets a name for the new shape from the user.
Input: names vector reference, name string reference.
Output: none.
*/
void nameInputException::getName(vector<string>& namesVector, string& name)
{
	bool getInputAgain = false;

	do
	{
		try // Get name
		{
			getInputAgain = false;
			std::cin >> name;
			searchName(namesVector, name); // Check if the name has already been entered
		}
		catch (std::exception & e) // If name is invalid (has been entered already), get name again
		{
			getInputAgain = true;
			std::cout << e.what() << std::endl;
		}
	} while (getInputAgain);

	namesVector.push_back(name); // Push the valid name into the names vector
}