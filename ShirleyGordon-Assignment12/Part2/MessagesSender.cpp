#include "MessagesSender.h"

std::atomic<bool> exitProg = false;
std::mutex uMtx;
std::mutex mMtx;
std::mutex mExit;
std::condition_variable cond;
std::condition_variable exitCond;

/*
Function gets a new user's name, and adds it to the vector of users
if it's not already in it.
Input: users vector reference.
Output: none.
*/
void MessagesSender::signIn(std::vector<string>& users)
{
	string newName = "";
	std::vector<string>::iterator usersIt = users.begin();
	std::unique_lock<std::mutex> usersMtx(uMtx, std::defer_lock);

	// Get new user's name.
	cout << "Please enter your name: " << endl;
	cin >> newName;

	usersMtx.lock(); // Lock users mutex.

	// Search for the new name in the users vector.
	usersIt = std::find(users.begin(), users.end(), newName);

	// If the name isn't in the vector, add the new user.
	if (usersIt == users.end())
	{
		users.push_back(newName);
		usersMtx.unlock(); // Unlock users mutex.
	}
	else // If the name was found in the vector, the user already exists.
	{
		usersMtx.unlock(); // Unlock users mutex.
		cerr << "Username already exists. Please try again." << endl;
		system("PAUSE");
	}
}

/*
Function gets a user's name, and removes it from the vector of users
if it's in it.
Input: users vector reference.
Output: none.
*/
void MessagesSender::signOut(std::vector<string>& users)
{
	string name = "";
	std::vector<string>::iterator it = users.begin();
	std::unique_lock<std::mutex> usersMtx(uMtx, std::defer_lock);

	// Get user's name.
	cout << "Please enter your name: " << endl;
	cin >> name;

	usersMtx.lock(); // Lock users mutex.

	// Remove name from vector.
	users.erase(std::remove(users.begin(), users.end(), name), users.end());

	usersMtx.unlock(); // Unlock users mutex.
}

/*
Function prints all the connected users.
Input: users vector reference.
Output: none.
*/
void MessagesSender::connectedUsers(const std::vector<string>& users)
{
	std::unique_lock<std::mutex> usersMtx(uMtx, std::defer_lock);

	usersMtx.lock(); // Lock users mutex.

	// Print all the user names.
	if (users.size() > 0)
	{
		std::copy(users.begin(), users.end(), std::ostream_iterator<string>(std::cout, "\n"));
		usersMtx.unlock(); // Unlock users mutex.
	}
	else // If there are no connected users, print message.
	{
		usersMtx.unlock(); // Unlock users mutex.
		cout << "There are currently no connected users." << endl;
	}

	system("PAUSE");
}

/*
Function checks if the file is empty.
Input: reference to file.
Output: true if file is empty, false otherwise.
*/
bool MessagesSender::isEmpty(std::fstream& file)
{
	return file.peek() == std::ifstream::traits_type::eof();
}

/*
Function prints the program's menu and gets choice input from user.
Input: reference to vector containing the names of the connected users.
Output: none.
*/
void MessagesSender::menu(std::vector<string>& users)
{
	int choice = 0;

	while (exitProg == false)
	{
		system("CLS"); // Clear the screen.

		// Print menu.
		cout << "Welcome to Shirley's Message Sender!" << endl
			<< "1. Sign in" << endl
			<< "2. Sign out" << endl
			<< "3. Print connected users" << endl
			<< "4. Exit" << endl;

		// Get user's choice.
		inputException::getNumericInput(choice);

		// Do what the user asked.
		switch (choice)
		{
			case SIGNIN:

				signIn(users);

				break;

			case SIGNOUT:

				signOut(users);

				break;

			case CONN_USERS:

				connectedUsers(users);

				break;

			case EXIT:

				exitCond.notify_one(); // Notify the queueMessages thread to stop waiting, and terminate.
				exitProg = true;

				break;

			default:

				break;
		}		
	}
}

/*
Function reads a data file containing messages, and puts them into a queue.
Input: messages vector reference.
Output: none.
*/
void MessagesSender::queueMessages(std::queue<string>& messages)
{
	std::fstream dataFile;
	std::stringstream data;
	string line;

	while (!exitProg)
	{
		dataFile.open("data.txt");

		if (dataFile.is_open())
		{
			while (!isEmpty(dataFile))
			{
				data << dataFile.rdbuf(); // Read file into string stream.
				dataFile.close();

				// Open file for truncating, which will delete the content of the file.
				dataFile.open("data.txt", std::ofstream::out | std::ofstream::trunc);
				dataFile.close();

				while (std::getline(data, line, '\n') && !exitProg) // Get each line in the data stringstream while possible. 
				{
					std::unique_lock<std::mutex> messagesMtx(mMtx);

					messages.push(line); // Push line into messages queue.

					messagesMtx.unlock();

					cond.notify_one(); // Notify other thread that the messages queue isn't empty.
				}				

				data.str(string()); // Clear the stringstream.
			}

			// Sleep for one minute.
			if (!exitProg)
			{
				std::unique_lock<std::mutex> exitMtx(mExit);
				exitCond.wait_for(exitMtx, std::chrono::minutes(1));
			}
		}
		else // Couldn't open data file.
		{
			cout << "Error opening data file." << endl;
			system("PAUSE");
			exitProg = true; // Signal all threads to exit.
		}
	}

	cond.notify_one(); // In order to allow the other thread to terminate.
}

/*
Function sends messages to all the connected users (writes them to output.txt file).
Input: messages queue reference, users vector reference.
Output: none.
*/
void MessagesSender::sendMessages(std::queue<string>& messages, const std::vector<string>& users)
{
	unsigned int i = 0, usersNum = 0;
	std::ofstream file("output.txt");
	std::unique_lock<std::mutex> usersMtx(uMtx, std::defer_lock);

	while (!exitProg)
	{
		if (file.is_open())
		{
			while (!messages.empty() && !exitProg)
			{
				usersMtx.lock();
				usersNum = users.size();
				usersMtx.unlock();

				for (i = 0; i < usersNum && !exitProg; i++)
				{
					// Wait for other thread to push a message into the queue.
					std::unique_lock<std::mutex> messagesMtx(mMtx);
					cond.wait(messagesMtx, [&]() {return !messages.empty() || exitProg; }); // Wait for condition variable to lock the messages mutex.
					
					if (!exitProg) // If the program should exit, don't continue.
					{
						usersMtx.lock();

						// Write the first message in the queue to each user.
						file << users[i] << ": " << messages.front() << endl;

						usersMtx.unlock();
					}
					messagesMtx.unlock();
				}

				mMtx.lock();
				messages.pop(); // Once done writing the first message, pop it from the queue.
				mMtx.unlock();
			}
		}
		else // Couldn't open output file.
		{
			cout << "Error opening output file." << endl;
			system("PAUSE");

			// Signal all threads to exit.
			exitProg = true; 
			exitCond.notify_one();
		}
	}
	
	file.close();
}