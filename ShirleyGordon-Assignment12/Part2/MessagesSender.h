#pragma once
#include <vector>
#include <iostream>
#include <queue>
#include <string>
#include <algorithm>
#include "inputException.h"
#include <iterator>
#include <mutex>
#include <chrono>
#include <condition_variable>
#include <fstream>
#include <atomic>
#include <sstream>

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::cin;

enum options : int {SIGNIN = 1, SIGNOUT, CONN_USERS, EXIT};

class MessagesSender
{
private:
	static void signIn(std::vector<string>& users);
	static void signOut(std::vector<string>& users);
	static void connectedUsers(const std::vector<string>& users);
	static bool isEmpty(std::fstream& file);

public:
	static void menu(std::vector<string>& users);
	static void queueMessages(std::queue<string>& messages);
	static void sendMessages(std::queue<string>& messages, const std::vector<string>& users);
};