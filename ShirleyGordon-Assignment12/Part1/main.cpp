#include "threads.h"

int main()
{
	std::ofstream file;
	
	cout << endl << "Test writing primes to file:" << endl;
	cout << "0-1000" << endl;
	callWritePrimesMultipleThreads(0, 1000, "primes1.txt", 2);

	cout << endl << "0-100000" << endl;
	callWritePrimesMultipleThreads(0, 100000, "primes2.txt", 5);

	cout << endl << "0-1000000" << endl;
	callWritePrimesMultipleThreads(0, 1000000, "primes3.txt", 10);
	
	system("pause");
	return 0;
}