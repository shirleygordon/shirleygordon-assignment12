#include <string>
#include <fstream>
#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>
#include <vector>

using std::cout;
using std::cin;
using std::endl;
using std::string;

#define MIN_DIVISOR 2

void writePrimesToFile(int begin, int end, std::ofstream& file);
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N);