#include "threads.h"

std::mutex primesCriticalSection;

/*
Function writes all the prime numbers in range begin-end into the file.
Input: begin, end, reference to file object.
Output: none.
*/
void writePrimesToFile(int begin, int end, std::ofstream& file)
{
	int i = 0, divisor = MIN_DIVISOR;
	bool isPrime = true, openedInFunc = false;
	string filePath = "";

	// Check if ofstream is associated with a specific file.
	if (!file.is_open())
	{
		// If not, ask the user to enter file path and open the file.
		cout << "Please enter file path: " << endl;
		cin >> filePath;
		file.open(filePath);
		openedInFunc = true; // Means the file has been opened in this function, so it will be closed in this function too.
	}

	// If the file is open, write the prime numbers into it.
	if (file.is_open())
	{
		if (begin <= end)
		{
			// Check each number in range begin-end.
			for (i = begin; i <= end; i++)
			{
				isPrime = true;

				// Divide the current number by all the numbers from 2 to its square root,
				// in order to check if it's a prime number.
				for (divisor = MIN_DIVISOR; divisor <= sqrt(i) && isPrime; divisor++)
				{
					// If it can be divided without a remainder, it's not a prime number.
					if (i % divisor == 0)
					{
						isPrime = false;		// Stop checking.
					}
				}
				
				if (isPrime && i != 0 && i != 1) // If the number is a prime number (0 and 1 aren't prime numbers),
				{
					/*try
					{*/
						primesCriticalSection.lock();
						file << i << endl;	// Write the number to file.
						primesCriticalSection.unlock();
					/*}
					catch (...)
					{
						file << i << endl;	// Write the number to file.
					}*/
				}
			}
		}		
		else
		{
			cout << "Error! Invalid range: begin is bigger than end." << endl;
		}

		// If the file has been opened in this function, it will also be closed in this function.
		if (openedInFunc)
		{
			file.close(); // Close the file when done.
		}
		
	}
	else // If the file couldn't be opened, an error occurred.
	{
		cout << "Error opening file. Please try again." << endl;
	}
}

/*
Function divides the given range (begin-end) to n ranges of equal length.
Then, it creates n threads, each of them runs the writePrimesToFile function on its range.
Function prints the total runtime of all threads.
*/
void callWritePrimesMultipleThreads(int begin, int end, std::string filePath, int N)
{
	unsigned int mainRangeLen = 0, smallRangeLen = 0, i = 0;
	int newBegin = 0;
	std::vector<std::thread> threads;
	std::ofstream file;
	std::chrono::time_point<std::chrono::high_resolution_clock> start, stop;
	std::chrono::microseconds duration;
	std::thread tempThread;

	file.open(filePath);

	if (file.is_open())
	{
		mainRangeLen = abs(end - begin) + 1; // Calculate the length of the main range.

		if (begin <= end)
		{
			// The length of the main range is can't be smaller than the amount of ranges to divide it by.
			if (N > mainRangeLen)
			{
				cout << "Error! The range " << begin << "-" << end << " can't be divided into " << N << " small ranges." << endl;
			}
			else
			{
				smallRangeLen = mainRangeLen / N; // The length of each small range.
				newBegin = begin;

				// Get starting timepoint.
				start = std::chrono::high_resolution_clock::now();

				// Create N threads, each thread with a smaller range.
				for (i = 0; i < N; i++)
				{
					// Create a new thread and move it into the threads vector.
					if (i == N - 1 && mainRangeLen % MIN_DIVISOR != 0) // If it's the last iteration of the loop, and the main range length is an odd number,
					{
						tempThread = std::thread(writePrimesToFile, newBegin, end, std::ref(file)); // Set the end to the original end (otherwise it will be the original end - 1).
					}
					else
					{
						tempThread = std::thread(writePrimesToFile, newBegin, newBegin + smallRangeLen - 1, std::ref(file));
					}					
					
					threads.push_back(std::move(tempThread));  // use the move function so that tempThread doesn't hold the current thread anymore.
					newBegin += smallRangeLen; // Set the beginning of the next thread.
				}

				// Join all the threads.
				for (i = 0; i < N; i++)
				{
					threads[i].join();
				}

				// Get ending timepoint.
				stop = std::chrono::high_resolution_clock::now();

				// Substart timepoints to get durarion, and print it.
				duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
				cout << "Total time taken by threads: " << duration.count() << " microseconds." << endl;
			}

			file.close();
		}
		else
		{
			cout << "Error! Invalid range: begin is bigger than end." << endl;
		}
	}
	else // File couldn't be opened.
	{
		cout << "Error opening file. Please try again." << endl;
	}
}














